import { SeasonFunc } from "../components/model-settings";
import { computed, observable, action } from "mobx";

export interface ModelData {
  trendCoeff: number;
  seasonFunc: SeasonFunc;
  seasonCoeff: number;
  seasonFuncCoeff: number;
  randomCoeff: number;
  constCoeff: number;
  min: number;
  max: number;
  items: number[];
}

export class ModelSettingsStore {
  constructor(data?: ModelData) {
    if(data != null) {
      this.trendCoeff = data.trendCoeff;
    this.seasonFunc = data.seasonFunc;
    this.seasonCoeff = data.seasonCoeff;
    this.seasonFuncCoeff = data.seasonFuncCoeff;
    this.randomCoeff = data.randomCoeff;
    this.constCoeff = data.constCoeff;
    this.min = data.min;
    this.max = data.max;
    this.series = data.items;
    }
  }

  @observable trendCoeff = 0;
  @observable seasonFunc = SeasonFunc.sin;
  @observable seasonCoeff = 0;
  @observable seasonFuncCoeff = 1;
  @observable randomCoeff = 0;
  @observable constCoeff = 0;
  @observable min = 0;
  @observable max = 200;
  @observable series = [];

  @computed
  get xValues() {
    const xValues = [];
    for (let t = this.min; t <= this.max; t++) {
      xValues.push(t);
    }
    return xValues;
  }

  @action.bound
  public setTrendCoeff(value) {
    this.trendCoeff = value;
  }

  @action.bound
  public setSeasonFunc(value) {
    this.seasonFunc = value;
  }

  @action.bound
  public setSeasonCoeff(value) {
    this.seasonCoeff = value;
  }

  @action.bound
  public setSeasonFuncCoeff(value) {
    this.seasonFuncCoeff = value;
  }

  @action.bound
  public setRandomCoeff(value) {
    this.randomCoeff = value;
  }

  @action.bound
  public setConstCoeff(value) {
    this.constCoeff = value;
  }

  @action.bound
  public setMin(value) {
    this.min = value;
  }

  @action.bound
  public setMax(value) {
    this.max = value;
  }

  @action.bound
  public generateSeries() {
    const series = [];
    for (let t = this.min; t <= this.max; t++) {
      const trend = this.trendCoeff * t;
      const season =
        this.seasonCoeff *
        (this.seasonFunc === SeasonFunc.sin
          ? Math.sin(this.seasonFuncCoeff * t)
          : Math.cos(this.seasonFuncCoeff * t));
      const random = this.randomCoeff * ((Math.random() - 0.5) / 0.5);
      series.push(trend + season + random + this.constCoeff);
    }
    this.series = series;
  }

  @action.bound
  addAnomaly(anomalyModelSettings: ModelSettingsStore) {
    anomalyModelSettings.generateSeries();
    const {min, max} = anomalyModelSettings;
    for(let i = min; i <= max; i++) {
      this.series[i] = anomalyModelSettings.series[i - min];
    }
  }
}
