import { Form, InputNumber, Select } from "antd";
import { ModelSettingsStore } from "../stores/model-settings-store";
import * as React from "react";
import {observer} from "mobx-react";

const Option = Select.Option;

export enum SeasonFunc {
  sin = "sin",
  cos = "cos"
}

export interface ModelSettingsProps {
  modelSettings: ModelSettingsStore;
  disabled?: boolean;
}

@observer
export class ModelSettings extends React.Component<ModelSettingsProps> {
  render() {
    const { modelSettings, disabled } = this.props;
    const {
      trendCoeff,
      seasonFunc,
      seasonCoeff,
      seasonFuncCoeff,
      randomCoeff,
      constCoeff,
      min,
      max
    } = modelSettings;

    const inputStyle = {
      width: "100%"
    };

    return (
      <Form style={{ padding: 20 }}>
        <Form.Item label={"t начальное"}>
          <InputNumber
            style={inputStyle}
            value={min}
            onChange={modelSettings.setMin}
            disabled={disabled}
          />
        </Form.Item>
        <Form.Item label={"t конечное"}>
          <InputNumber
            style={inputStyle}
            value={max}
            onChange={modelSettings.setMax}
            disabled={disabled}
          />
        </Form.Item>
        <Form.Item label={"Коэффициент тренда"}>
          <InputNumber
            style={inputStyle}
            value={trendCoeff}
            step={0.1}
            onChange={modelSettings.setTrendCoeff}
            disabled={disabled}
          />
        </Form.Item>
        <Form.Item label={"Коэффициент сезонности"}>
          <InputNumber
            style={inputStyle}
            value={seasonCoeff}
            step={0.1}
            onChange={modelSettings.setSeasonCoeff}
            disabled={disabled}
          />
        </Form.Item>
        <Form.Item label={"Функция сезонности"}>
          <Select
            style={inputStyle}
            value={seasonFunc}
            onChange={modelSettings.setSeasonFunc}
            disabled={disabled}
          >
            {Object.keys(SeasonFunc).map(func => (
              <Option key={SeasonFunc[func]}>{SeasonFunc[func]}</Option>
            ))}
          </Select>
        </Form.Item>
        <Form.Item label={"Коэффициент периода"}>
          <InputNumber
            style={inputStyle}
            value={seasonFuncCoeff}
            step={0.1}
            onChange={modelSettings.setSeasonFuncCoeff}
            disabled={disabled}
          />
        </Form.Item>
        <Form.Item label={"Коэффициент случайности"}>
          <InputNumber
            style={inputStyle}
            value={randomCoeff}
            step={0.1}
            onChange={modelSettings.setRandomCoeff}
            disabled={disabled}
          />
        </Form.Item>
        <Form.Item label={"Константа"}>
          <InputNumber
            style={inputStyle}
            value={constCoeff}
            step={0.1}
            onChange={modelSettings.setConstCoeff}
            disabled={disabled}
          />
        </Form.Item>
      </Form>
    );
  }
}
