import ReactEcharts from "echarts-for-react";
import * as React from "react";
import {observer} from "mobx-react";
import {computed} from "mobx";

export interface ChartProps {
  xAxis: number[];
  series: number[][];
}

@observer
export class Chart extends React.Component<ChartProps> {

  echartStyle = {
    height: "100%",
    width: "100%",
    overflow: "hidden",
    whiteSpace: "nowrap",
    textOverflow: "ellipsis"
  };

  @computed
  get option() {
    const {xAxis, series} = this.props;

    return {
      tooltip: {
        trigger: "axis",
        axisPointer: {
          type: "shadow"
        }
      },
      grid: {
        left: "3%",
        right: "4%",
        bottom: "3%",
        containLabel: true
      },
      xAxis: [
        {
          type: "category",
          data: xAxis,
          axisLabel: {
            rotate: 45
          }
        }
      ],
      yAxis: [
        {
          type: "value"
        }
      ],
      series: series.map((ser, index) => ({
        name: `Ряд ${index}`,
        type: "line",
        data: ser
      }))
    };
  }

  render() {
    return (
      <ReactEcharts
        option={this.option}
        style={this.echartStyle}
        notMerge={true}
      />
    );
  }
}
