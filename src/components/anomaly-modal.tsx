import { ModelSettings, SeasonFunc } from "./model-settings";
import { ModelSettingsStore } from "../stores/model-settings-store";
import { observable } from "mobx";
import Modal from "antd/lib/modal/Modal";
import * as React from "react";
import {observer} from "mobx-react";
import {autobind} from "core-decorators";

export interface AnomalyModalProps {
  visible: boolean;
  onOk: (series: ModelSettingsStore) => void;
  onCancel: () => void;
}

@observer
export class AnomalyModal extends React.Component<AnomalyModalProps> {
  @observable
  currentModelSettings = new ModelSettingsStore();

  @autobind
  addAnomaly() {
    this.props.onOk(this.currentModelSettings);
  }

  render() {
    const { visible, onOk, onCancel } = this.props;

    return (
      <Modal
        title="Добавление аномалии"
        visible={visible}
        onOk={this.addAnomaly}
        onCancel={onCancel}
      >
        <ModelSettings modelSettings={this.currentModelSettings} />
      </Modal>
    );
  }
}
