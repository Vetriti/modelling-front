import {MainMenu, MenuItems} from "./main-menu";
import {Series} from "./series";
import {SeriesTable} from "./series-table";
import * as React from "react";
import {observable, action, computed} from "mobx";
import {observer} from "mobx-react";
import {autobind} from "core-decorators";
import {ModelSettingsStore} from "../stores/model-settings-store";

const axios = require("axios");

@observer
export class Main extends React.Component {
  @observable
  currentMenuKey: MenuItems = MenuItems.generate;

  @observable
  currentModelSettings = new ModelSettingsStore();

  @observable
  data = [];

  @action.bound
  getData() {
    return axios
      .post("/api/series/getAll")
      .then(result => (this.data = result.data));
  }

  @action.bound
  onDelete = record => {
    axios.post(`/api/series/delete/${record.id}`);
    this.data = this.data.filter(item => item.id !== record.id);
  };

  @action.bound
  onMenuSelect(item) {
    this.currentMenuKey = item.key;
    if (item.key === MenuItems.get) {
      this.getData();
    }
  }

  @computed
  get form() {
    switch (this.currentMenuKey) {
      case MenuItems.generate:
        return <Series modelSettings={this.currentModelSettings} disabled={false}/>;
      case MenuItems.get:
        return (
          <SeriesTable
            data={this.data}
            onDelete={this.onDelete}
          />
        );
    }
  }

  render() {
    return (
      <React.Fragment>
        <MainMenu
          selectedKeys={[this.currentMenuKey]}
          onSelect={this.onMenuSelect}
        />
        <div style={{height: "calc(100% - 48px)"}}>{this.form}</div>
      </React.Fragment>
    );
  }
}
