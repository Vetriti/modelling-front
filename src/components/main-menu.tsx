import {Icon, Menu} from "antd";
import * as React from "react";
import {observer} from "mobx-react";

const {SubMenu} = Menu;

export enum MenuItems {
  generate = "generate",
  get = "get"
}

export const MenuNames = {
  generate: "Создать",
  get: "Ряды",
};

export interface MainMenuProps {
  selectedKeys: string[];
  onSelect: (item) => void;
}

@observer
export class MainMenu extends React.Component<MainMenuProps> {
  render() {
    return (
      <Menu
        style={{width: "100%"}}
        selectedKeys={this.props.selectedKeys}
        onSelect={this.props.onSelect}
        mode={"horizontal"}
      >
        {Object.keys(MenuItems).map(key =>
          <Menu.Item key={MenuItems[key]}>
            <Icon type="appstore"/>
            {MenuNames[key]}
          </Menu.Item>
        )}
      </Menu>
    );
  }
}
