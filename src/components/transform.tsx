import { Table, InputNumber, Form } from "antd";
import * as React from "react";
import { observer } from "mobx-react";
import { ModelSettingsStore } from "../stores/model-settings-store";
import { ViewType } from "./series";
import { observable, action, computed } from "mobx";
import { Chart } from "./chart";

const axios = require("axios");

export interface TransformReport {
  matrix: number[][];
  deFuzzySeries: number[];
  trendSeries: number[];
  tendency: string;
  trendTendency: string;
  tendencyIntensity: number;
  mape: number;
}

export interface TransformProps {
  modelSettings: ModelSettingsStore;
  transformType: ViewType;
}

@observer
export class Transform extends React.Component<TransformProps> {
  @observable
  private lingNum = 5;

  @observable
  private transform: TransformReport;

  @computed
  get transformMatrix(): number[][] {
    return this.transform && this.transform.matrix;
  }

  @computed
  get deFuzzySeries(): number[] {
    return (
      this.transform &&
      this.transform.deFuzzySeries &&
      this.transform.deFuzzySeries.slice()
    );
  }

  @action.bound
  getTransform() {
    const {
      trendCoeff,
      seasonFunc,
      seasonCoeff,
      seasonFuncCoeff,
      randomCoeff,
      constCoeff,
      min,
      max,
      series
    } = this.props.modelSettings;
    const data = {
      trendCoeff,
      seasonFunc,
      seasonCoeff,
      seasonFuncCoeff,
      randomCoeff,
      constCoeff,
      min,
      max,
      items: series
    };
    axios
      .post(`/api/series/nlx/${this.lingNum}`, data)
      .then(result => (this.transform = result.data));
  }

  @action.bound
  setLingNum(value: number) {
    this.lingNum = value;
    this.getTransform();
  }

  componentDidMount() {
    this.getTransform();
  }

  render() {
    if (!this.transform) {
      return null;
    }
    const { modelSettings, transformType } = this.props;

    let columns = [
      { title: "t", dataIndex: "num", key: "num" },
      { title: "Значение", dataIndex: "value", key: "value" }
    ];

    const termColumns =
      this.transformMatrix &&
      this.transformMatrix[0].map((col, index) => ({
        title: `A${index}`,
        dataIndex: `A${index}`,
        key: `A${index}`
      }));

    switch (transformType) {
      case ViewType.NFXM:
        if (termColumns != null) {
          columns.push(...termColumns);
        }
        break;
      case ViewType.NFXV:
        columns.push({ title: "Вектор", dataIndex: "vector", key: "vector" });
        if (termColumns != null) {
          columns.push(...termColumns);
        }
        break;
      case ViewType.NLX:
        columns.push({ title: "Терм", dataIndex: "term", key: "term" });
        break;
      case ViewType.NLFX:
        columns.push(
          { title: "Терм", dataIndex: "term", key: "term" },
          {
            title: "Значение терма",
            dataIndex: "termValue",
            key: "termValue"
          }
        );
        break;
    }

    const tableData =
      this.transformMatrix &&
      this.transformMatrix.map((row, rowIndex) =>
        row.reduce(
          (acc, val, index) => ({
            ...acc,
            [`A${index}`]: val,
            term: acc.termValue < val ? `A${index}` : acc.term,
            termValue: acc.termValue < val ? val : acc.termValue
          }),
          {
            key: rowIndex,
            num: modelSettings.min + rowIndex,
            value: modelSettings.series[rowIndex],
            vector: `y${rowIndex}`,
            term: "",
            termValue: 0
          }
        )
      );

    return (
      <div style={{ height: "100%", overflow: "auto" }}>
        <div>{`Тенденция: ${this.transform.tendency}`}</div>
          <div>{`Тренд: ${this.transform.trendTendency}`}</div>
        <Form.Item label={"Количество лингвистических переменных"}>
          <InputNumber value={this.lingNum} onChange={this.setLingNum} />
        </Form.Item>
        {transformType === ViewType.NNX ? (
          <React.Fragment>
            <div>{`MAPE: ${this.transform.mape}`}</div>
            <Chart
              xAxis={modelSettings.xValues}
              series={[modelSettings.series.slice(), this.deFuzzySeries, this.transform.trendSeries]}
            />
          </React.Fragment>
        ) : (
          (columns && columns.length && (
            <Table
              style={{ padding: 20 }}
              columns={columns}
              pagination={false}
              dataSource={tableData}
            />
          )) ||
          null
        )}
      </div>
    );
  }
}
