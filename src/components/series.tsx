import { Chart } from "./chart";
import { ModelSettings } from "./model-settings";
import { Button, Radio } from "antd";
import { SeriesDataTable } from "./series-data-table";
import { AnomalyModal } from "./anomaly-modal";
import { observable, action } from "mobx";
import { ModelSettingsStore } from "../stores/model-settings-store";
import * as React from "react";
import { observer } from "mobx-react";
import { autobind } from "core-decorators";
import { Transform } from "./transform";

const axios = require("axios");

export enum ViewType {
  graph = "graph",
  table = "table",
  NFXM = "NFXM",
  NFXV = "NFXV",
  NLX = "NLX",
  NLFX = "NLFX",
  NNX = "NNX",
}

export interface SeriesProps {
  modelSettings: ModelSettingsStore;
  disabled: boolean;
}

@observer
export class Series extends React.Component<SeriesProps> {
  @observable
  currentView = ViewType.graph;

  @observable
  showAnomalyModal = false;

  @action.bound
  toggleAnomalyModal() {
    this.showAnomalyModal = !this.showAnomalyModal;
  }

  @action.bound
  addAnomaly(newModel: ModelSettingsStore) {
    this.toggleAnomalyModal();
    this.props.modelSettings.addAnomaly(newModel);
  }

  @autobind
  saveSeries() {
    const {
      trendCoeff,
      seasonFunc,
      seasonCoeff,
      seasonFuncCoeff,
      randomCoeff,
      constCoeff,
      min,
      max,
      series
    } = this.props.modelSettings;
    const data = {
      trendCoeff,
      seasonFunc,
      seasonCoeff,
      seasonFuncCoeff,
      randomCoeff,
      constCoeff,
      min,
      max,
      items: series
    };
    return axios.post("/api/series/create", data);
  }

  @action.bound
  changeCurrentViewType(e) {
    this.currentView = e.target.value;
  }

  get currentModelView() {
    const { xValues, series, min } = this.props.modelSettings;
    switch (this.currentView) {
      case ViewType.graph:
        return <Chart xAxis={xValues} series={[series.slice()]} />;
      case ViewType.table:
        return <SeriesDataTable min={min} items={series.slice()} />;
      default:
        return (
          <Transform
            modelSettings={this.props.modelSettings}
            transformType={this.currentView}
          />
        );
    }
  }

  render() {
    const { modelSettings, disabled } = this.props;
    const {
      trendCoeff,
      seasonFunc,
      seasonCoeff,
      seasonFuncCoeff,
      randomCoeff,
      constCoeff,
      generateSeries
    } = modelSettings;

    return (
      <div style={{ height: "100%" }}>
        {!disabled ? (
          <div
            style={{
              width: "20%",
              height: "100%",
              overflowY: "auto",
              float: "left"
            }}
          >
            <ModelSettings modelSettings={modelSettings} disabled={disabled} />
            <Button onClick={generateSeries}>Генерировать</Button>
            <Button onClick={this.toggleAnomalyModal}>Добавить аномалию</Button>
            <Button onClick={this.saveSeries}>Сохранить</Button>
          </div>
        ) : null}
        <div
          style={{
            width: !disabled ? "80%" : "100%",
            height: "100%",
            float: "left"
          }}
        >
          <Radio.Group
            value={this.currentView}
            onChange={this.changeCurrentViewType.bind(this)}
            buttonStyle="solid"
          >
            <Radio.Button value={ViewType.graph}>График</Radio.Button>
            <Radio.Button value={ViewType.table}>Таблица</Radio.Button>
            {disabled ? (
              <React.Fragment>
                <Radio.Button value={ViewType.NFXM}>NFXM</Radio.Button>
                <Radio.Button value={ViewType.NFXV}>NFXV</Radio.Button>
                <Radio.Button value={ViewType.NLX}>NLX</Radio.Button>
                <Radio.Button value={ViewType.NLFX}>NLFX</Radio.Button>
                <Radio.Button value={ViewType.NNX}>NNX</Radio.Button>
              </React.Fragment>
            ) : null}
          </Radio.Group>
          <div style={{ fontSize: "35px" }}>
            {`${trendCoeff}*t + ${seasonCoeff}*${seasonFunc}(${seasonFuncCoeff}*t) + ${randomCoeff}*e(t) + ${constCoeff}`}
          </div>
          <div style={{ height: "calc(100% - 52px - 32px)" }}>
            {this.currentModelView}
          </div>
        </div>
        <AnomalyModal
          visible={this.showAnomalyModal}
          onCancel={this.toggleAnomalyModal}
          onOk={this.addAnomaly}
        />
      </div>
    );
  }
}
