import { Popconfirm, Table } from "antd";
import { SeasonFunc } from "./model-settings";
import { Series } from "./series";
import * as React from "react";
import { observable, action } from "mobx";
import { ModelSettingsStore, ModelData } from "../stores/model-settings-store";
import { observer } from "mobx-react";

export const HasProperty = {
  true: "есть",
  false: "нет"
};

export interface SeriesTableProps {
  data: ModelData[];
  onDelete: (val) => void;
}

@observer
export class SeriesTable extends React.Component<SeriesTableProps> {
  @observable
  selectedModelSettings: ModelSettingsStore;

  @action.bound
  setSeries(index) {
    this.selectedModelSettings = new ModelSettingsStore(this.props.data[index]);
  }

  columns = [
    {
      title: "Начальная точка",
      dataIndex: "min",
      key: "min"
    },
    {
      title: "Конечная точка",
      dataIndex: "max",
      key: "max"
    },
    {
      title: "Коэфф. тренда",
      dataIndex: "trendCoeff",
      key: "trendCoeff",
      filters: Object.keys(HasProperty).map(value => ({
        text: HasProperty[value],
        value
      })),
      onFilter: (value, record) =>
        (record.trendCoeff !== 0 && value === HasProperty.true) ||
        (record.trendCoeff === 0 && value === HasProperty.false)
    },
    {
      title: "Коэфф. сезонности",
      dataIndex: "seasonCoeff",
      key: "seasonCoeff",
      filters: Object.keys(HasProperty).map(value => ({
        text: HasProperty[value],
        value
      })),
      onFilter: (value, record) =>
        (record.seasonCoeff !== 0 && value === HasProperty.true) ||
        (record.seasonCoeff === 0 && value === HasProperty.false)
    },
    {
      title: "Функция сезонности",
      dataIndex: "seasonFunc",
      key: "seasonFunc",
      filters: Object.keys(SeasonFunc).map(func => ({
        text: func,
        value: func
      })),
      onFilter: (value, record) => record.seasonFunc === value
    },
    {
      title: "Коэфф. периода сезонности",
      dataIndex: "seasonFuncCoeff",
      key: "seasonFuncCoeff"
    },
    {
      title: "Коэфф. случайности",
      dataIndex: "randomCoeff",
      key: "randomCoeff",
      filters: Object.keys(HasProperty).map(value => ({
        text: HasProperty[value],
        value
      })),
      onFilter: (value, record) =>
        (record.randomCoeff !== 0 && value === HasProperty.true) ||
        (record.randomCoeff === 0 && value === HasProperty.false)
    },
    {
      title: "Константа",
      dataIndex: "constCoeff",
      key: "constCoeff",
      filters: Object.keys(HasProperty).map(value => ({
        text: HasProperty[value],
        value
      })),
      onFilter: (value, record) =>
        (record.constCoeff !== 0 && value === HasProperty.true) ||
        (record.constCoeff === 0 && value === HasProperty.false)
    },
    {
      dataIndex: "operation",
      render: (text, record) => (
        <Popconfirm
          title="Удалить ряд?"
          onConfirm={() => this.props.onDelete(record)}
        >
          <a href="javascript:;">Удалить</a>
        </Popconfirm>
      )
    }
  ];

  render() {
    const { data } = this.props;

    return (
      <React.Fragment>
        {this.selectedModelSettings != null ? (
          <Series
            modelSettings={this.selectedModelSettings}
            disabled={true}
          />
        ) : (
          <div style={{ height: "100%", overflow: "auto" }}>
            <Table
              columns={this.columns}
              dataSource={data}
              onRow={(record, rowIndex) => ({
                onDoubleClick: () => this.setSeries(rowIndex)
              })}
            />
          </div>
        )}
      </React.Fragment>
    );
  }
}
