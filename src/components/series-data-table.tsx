import { Table } from "antd";
import * as React from "react";
import {observer} from "mobx-react";

export interface SeriesDataTableProps {
  min: number;
  items: number[];
}

@observer
export class SeriesDataTable extends React.Component<SeriesDataTableProps> {
  render() {
    const { min, items } = this.props;

    const columns = [
      {
        title: "t",
        dataIndex: "num",
        key: "num"
      },
      {
        title: "Значение ряда",
        dataIndex: "value",
        key: "value"
      }
    ];

    const tableData =
      items && items.map((item, index) => ({key: index, num: min + index, value: item }));

    return (
      <div style={{ height: "100%", overflow: "auto" }}>
        {(columns && columns.length && (
          <Table
            style={{ padding: 20 }}
            columns={columns}
            pagination={false}
            dataSource={tableData}
          />
        )) ||
          null}
      </div>
    );
  }
}
